# RealDigitalTask

This is a task for job application

# Task

## Story
As a customer
I want to maintain a shopping list by adding and removing articles,
to have a good overview about articles which I may want to buy later.

## Acceptance criteria:
- Products can be searched with a search field
- Each product can be added to a shopping list
- Shopping list can be viewed
- Products can be removed from shopping list
- Name of shopping lists can be defined and changed

## API connection:
- Client may restrict access to API. This can be bypassed.

## Design:
- User friendly. No specific restrictions.

# How to run

* Clone the repo:
`git clone https://walpurgisnacht@bitbucket.org/walpurgisnacht/realdigitaltask.git`

* Go inside the directory and install dependencies:
`cd realdigitaltask && npm install`

* Run in development mode: `npm start`
* Go to your browser: `http://localhost:4200/`

# How to use

* Create a List with the top right 'shopping car icon'.

* Use the top 'drumstick' icon to navigate to list, search and filter products.

* After adding products the 'shopping car icon' shows the list of shopping lists, you can click one list to show details.

* In the shopping list details view you can edit name and remove products if you have added some.

* In products list and in shopping list details as well, you can see the product details, in the first is showed by clicking the 'horizontal ellipsis' icon, and in the second it is possible just by clicking the name of the product.

# Tech Specifications

* Angular 7
* ngRX 7 to handle app state
* ngxPagination library
* no bootstrap for styling


# Contact

* Email me if you have questions: jdtorregrosas@gmail.com
