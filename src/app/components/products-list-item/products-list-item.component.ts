import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../models/Product.interface';

@Component({
  selector: 'app-products-list-item',
  templateUrl: './products-list-item.component.html',
  styleUrls: ['./products-list-item.component.scss']
})
export class ProductsListItemComponent implements OnInit {

  @Input()
  product: Product;
  baseUrl = 'https://api.efood.real.de/';
  showModal = false;
  showAddModal = false;

  constructor() { }

  ngOnInit() {
  }

  showDetail() {
    this.showModal = true;
  }

  showAddDetail() {
    this.showAddModal = true;
  }

}
