import { ShoppingListsService } from './../../services/shopping-lists/shopping-lists.service';
import { Observable } from 'rxjs';
import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { Product } from '../../models/Product.interface';
import { ShoppingList } from '../../models/ShoppingList.interface';

@Component({
  selector: 'app-add-to-list-modal',
  templateUrl: './add-to-list-modal.component.html',
  styleUrls: ['./add-to-list-modal.component.scss']
})
export class AddToListModalComponent implements OnInit {

  @Input()
  show: boolean;
  @Input()
  product: Product;
  @Output()
  showChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  list = '';
  lists$: Observable<ShoppingList[]>;
  baseUrl = 'https://api.efood.real.de/';

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeModal();
  }

  constructor(private shoppingListsService: ShoppingListsService) { }

  ngOnInit() {
    this.lists$ = this.shoppingListsService.shoppingLists$;
  }

  closeModal() {
    this.show = false;
    this.showChange.emit(this.show);
    this.list = '';
  }

  addProductToList() {
    this.shoppingListsService.addProductToList(this.list, this.product);
    this.closeModal();
  }

}
