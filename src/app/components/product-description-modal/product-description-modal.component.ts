import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { productsReducer } from '../../store/reducers/products.reducer';
import { Product } from '../../models/Product.interface';

@Component({
  selector: 'app-product-description-modal',
  templateUrl: './product-description-modal.component.html',
  styleUrls: ['./product-description-modal.component.scss']
})
export class ProductDescriptionModalComponent implements OnInit {

  @Input()
  show: boolean;
  @Input()
  product: Product;

  @Output()
  showChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  baseUrl = 'https://api.efood.real.de/';

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.closeModal();
  }

  constructor() { }

  ngOnInit() {
  }

  closeModal() {
    this.show = false;
    this.showChange.emit(this.show);
  }

}
