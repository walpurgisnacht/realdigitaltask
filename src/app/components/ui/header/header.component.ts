import { ShoppingListsService } from './../../../services/shopping-lists/shopping-lists.service';
import { Observable } from 'rxjs';
import { Component, OnInit, HostListener } from '@angular/core';
import { ShoppingList } from '../../../models/ShoppingList.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  lists$: Observable<ShoppingList[]>;
  showLists = false;
  addList = false;
  newListName = '';

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.showLists = false;
  }

  constructor(private shoppingListsService: ShoppingListsService, private router: Router) { }

  ngOnInit() {
    this.lists$ = this.shoppingListsService.shoppingLists$;
  }

  closeShoppingLists() {
    this.showLists = false;
    this.clearNewListItem();
  }

  toggleLists() {
    this.showLists = !this.showLists;
  }

  clearNewListItem() {
    this.newListName = '';
    this.addList = false;
  }

  toggleAddList() {
    this.addList = !this.addList;
  }

  createList() {
    if (this.newListName) {
      this.shoppingListsService.createList(this.newListName);
      this.clearNewListItem();
    }
  }
  navigateToListDetail(id) {
    this.router.navigate([`list-detail/${id}`]);
    this.closeShoppingLists();
  }
  navigateToProducts() {
    this.router.navigate(['products']);
    this.closeShoppingLists();
  }
  navigateToHome() {
    this.router.navigate(['']);
    this.closeShoppingLists();
  }
}
