import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Product } from '../../models/Product.interface';

@Component({
  selector: 'app-shopping-list-products',
  templateUrl: './shopping-list-products.component.html',
  styleUrls: ['./shopping-list-products.component.scss']
})
export class ShoppingListProductsComponent implements OnInit {

  @Input()
  products: Product[];
  @Input()
  listId: string;
  @Output()
  productRemoved: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  removeProduct() {
    this.productRemoved.emit(true);
  }

}
