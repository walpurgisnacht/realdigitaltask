import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShoppingListProductsComponent } from './shopping-list-products.component';

describe('ShoppingListProductsComponent', () => {
  let component: ShoppingListProductsComponent;
  let fixture: ComponentFixture<ShoppingListProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShoppingListProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoppingListProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
