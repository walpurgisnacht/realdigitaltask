import { ShoppingListsService } from './../../services/shopping-lists/shopping-lists.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from '../../models/Product.interface';

@Component({
  selector: 'app-shopping-list-products-item',
  templateUrl: './shopping-list-products-item.component.html',
  styleUrls: ['./shopping-list-products-item.component.scss']
})
export class ShoppingListProductsItemComponent implements OnInit {

  @Input()
  product: Product;
  @Input()
  listId: string;
  @Output()
  removed: EventEmitter<boolean> = new EventEmitter<boolean>();
  showDescription = false;

  constructor(private shoppingListsService: ShoppingListsService) { }

  ngOnInit() {
  }

  removeProduct() {
    this.shoppingListsService.removeProductFromList(this.listId, this.product);
    this.removed.emit(true);
  }

}
