import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShoppingListProductsItemComponent } from './shopping-list-products-item.component';

describe('ShoppingListProductsItemComponent', () => {
  let component: ShoppingListProductsItemComponent;
  let fixture: ComponentFixture<ShoppingListProductsItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShoppingListProductsItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoppingListProductsItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
