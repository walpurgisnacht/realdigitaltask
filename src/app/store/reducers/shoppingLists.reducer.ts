import { ShoppingListsAction, ShoppingListsActionTypes } from './../actions/shoppingLists.actions';
import { ShoppingList } from '../../models/ShoppingList.interface';
import { v4 } from 'uuid';

export interface ShoppingListsState {
  shoppingLists: ShoppingList[];
}

export const shoppingListsInitialState: ShoppingListsState = {
  shoppingLists: []
};

export function shoppingListsReducer(
  state: ShoppingListsState = shoppingListsInitialState,
  action: ShoppingListsAction
): ShoppingListsState {
  switch (action.type) {
    case ShoppingListsActionTypes.CreateList:
      return {
        ...state,
        shoppingLists: [{ id: v4(), name: action.shoppingListName, products: [] }, ...state.shoppingLists]
      };
    case ShoppingListsActionTypes.AddProductToList:
      const updatedLists = state.shoppingLists.map(list => {
        let newList = { ...list };
        if (list.id === action.shoppingListId) {
          newList = { ...list, products: [...list.products, action.product] };
        }
        return newList;
      });
      return {
        ...state,
        shoppingLists: [...updatedLists]
      };
    case ShoppingListsActionTypes.RemoveProductFromList:
      const updatedRemovedProductsList = state.shoppingLists.map(list => {
        const newList = { ...list };
        if (list.id === action.shoppingListId) {
          let removed = false;
          newList.products = newList.products.filter(p => {
            if (p.code === action.product.code && !removed) {
              removed = true;
              return false;
            }
            return true;
          });
        }
        return newList;
      });
      return {
        ...state,
        shoppingLists: [...updatedRemovedProductsList]
      };
    case ShoppingListsActionTypes.ChangeListName:
      const updatedNameChangedList = state.shoppingLists.map(list => {
        const newList = { ...list };
        if (list.id === action.shoppingListId) {
          newList.name = action.shoppingListName;
        }
        return newList;
      });
      return {
        ...state,
        shoppingLists: [...updatedNameChangedList]
      };
    default:
      return state;
  }
}
