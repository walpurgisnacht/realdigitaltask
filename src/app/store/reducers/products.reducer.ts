import { ProductsActionTypes, ProductsAction } from './../actions/products.actions';
import { Product } from '../../models/Product.interface';

export interface ProductsState {
  products: Product[];
}

export const productsInitialState: ProductsState = {
  products: []
};

export function productsReducer(state: ProductsState = productsInitialState, action: ProductsAction) {
  switch (action.type) {
    case ProductsActionTypes.LoadProducts:
      return state;
    case ProductsActionTypes.LoadProductsSuccess:
      return {
        ...state,
        products: action.payload
      };
    default:
      return state;
  }
}
