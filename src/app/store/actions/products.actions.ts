import { Action } from '@ngrx/store';
import { Product } from '../../models/Product.interface';

export interface ProductsAction extends Action {
  searchTerm?: string;
  sort?: string;
  maxResults?: number;
  payload?: Product[];
  error?: string;
}

export enum ProductsActionTypes {
  LoadProducts = '[Products] Load products',
  LoadProductsSuccess = '[Products-effect] Load products success',
  LoadProductsError = '[Products-effect] Load products error'
}

export function loadProducts(searchTerm: string, sort: string, maxResults: number): ProductsAction {
  return {
    searchTerm,
    sort,
    maxResults,
    type: ProductsActionTypes.LoadProducts,
    payload: []
  };
}
