import { Action } from '@ngrx/store';
import { Product } from '../../models/Product.interface';

export interface ShoppingListsAction extends Action {
  shoppingListName?: string;
  shoppingListId?: string;
  product?: Product;
  error?: string;
}

export enum ShoppingListsActionTypes {
  CreateList = '[ShoppingLists] create list',
  AddProductToList = '[Shopping Lists] add product to list',
  RemoveProductFromList = '[Shopping Lists] remove product from list',
  ChangeListName = '[Shopping Lists] change list name'
}

export function createShoppingList(shoppingListName: string): ShoppingListsAction {
  return {
    shoppingListName,
    type: ShoppingListsActionTypes.CreateList
  };
}

export function addProductToShoppingList(shoppingListId: string, product: Product): ShoppingListsAction {
  return {
    product,
    shoppingListId,
    type: ShoppingListsActionTypes.AddProductToList,
  };
}

export function removeProductFromShoppingList(shoppingListId: string, product: Product) {
  return {
    product,
    shoppingListId,
    type: ShoppingListsActionTypes.RemoveProductFromList,
  };
}

export function changeListName(listId: string, listName: string) {
  return {
    shoppingListName: listName,
    shoppingListId: listId,
    type: ShoppingListsActionTypes.ChangeListName
  };
}
