import { ProductsActionTypes } from './../actions/products.actions';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, catchError, tap } from 'rxjs/operators';

@Injectable()
export class ProductsEffects {

  @Effect()
  loadProducts$ = this.actions$
    .pipe(
      ofType(ProductsActionTypes.LoadProducts),
      mergeMap(
        action =>
          this.http
            .get(
              `/api/products/search?query=${action['searchTerm']}:`
              + `${action['sort']}:category:1&currentPage=0&pageSize=${action['maxResults']}`)
            .pipe(
              map(response => ({ type: ProductsActionTypes.LoadProductsSuccess, payload: response['products'] })),
              catchError(() => of({ type: ProductsActionTypes.LoadProductsError }))
            ))
    );

  constructor(
    private actions$: Actions,
    private http: HttpClient
  ) { }
}
