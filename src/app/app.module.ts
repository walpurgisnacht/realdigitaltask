import { ProductsEffects } from './store/effects/products.effects';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductsComponent } from './pages/products/products.component';
import { ProductsListComponent } from './components/products-list/products-list.component';
import { ProductsListItemComponent } from './components/products-list-item/products-list-item.component';
import { HeaderComponent } from './components/ui/header/header.component';
import { FooterComponent } from './components/ui/footer/footer.component';
import { productsReducer } from './store/reducers/products.reducer';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { ProductDescriptionModalComponent } from './components/product-description-modal/product-description-modal.component';
import { shoppingListsReducer } from './store/reducers/shoppingLists.reducer';
import { AddToListModalComponent } from './components/add-to-list-modal/add-to-list-modal.component';
import { ListDetailComponent } from './pages/list-detail/list-detail.component';
import { HomeComponent } from './pages/home/home.component';
import { ShoppingListProductsComponent } from './components/shopping-list-products/shopping-list-products.component';
import { ShoppingListProductsItemComponent } from './components/shopping-list-products-item/shopping-list-products-item.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductsListComponent,
    ProductsListItemComponent,
    HeaderComponent,
    FooterComponent,
    ProductDescriptionModalComponent,
    AddToListModalComponent,
    ListDetailComponent,
    HomeComponent,
    ShoppingListProductsComponent,
    ShoppingListProductsItemComponent,
    NotFoundComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
    StoreModule.forRoot({ products: productsReducer, shoppingLists: shoppingListsReducer }),
    EffectsModule.forRoot([ProductsEffects]),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
