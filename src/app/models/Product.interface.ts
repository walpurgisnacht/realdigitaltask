export interface Product {
  assortmentGroup: string;
  brand: string;
  bulkyProduct: boolean;
  code: string;
  contentUnit: string;
  description: string;
  images: Image[];
  name: string;
  numberContentUnits: string;
  price: Price;
}

export interface Image {
  format: string;
  imageType: string;
  url: string;
}

export interface Price {
  basePrice: {
    basePrice: number;
    basePriceBaseFactor: number;
    basePriceUnit: string;
  };
  currencyIso: string;
  formattedValue: string;
  priceType: string;
  value: string;
}
