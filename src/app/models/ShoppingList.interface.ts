import { Product } from './Product.interface';

export interface ShoppingList {
  id: string;
  name: string;
  products: Product[];
}
