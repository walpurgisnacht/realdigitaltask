import { ShoppingListsState } from './../../store/reducers/shoppingLists.reducer';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { ShoppingList } from '../../models/ShoppingList.interface';
import {
  createShoppingList,
  addProductToShoppingList,
  removeProductFromShoppingList,
  changeListName
} from '../../store/actions/shoppingLists.actions';
import { Product } from '../../models/Product.interface';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListsService {

  shoppingLists$: Observable<ShoppingList[]>;

  constructor(private store: Store<{ shoppingLists: ShoppingListsState }>) {
    this.shoppingLists$ = this.store.select(state => state.shoppingLists.shoppingLists);
  }

  createList(name: string) {
    this.store.dispatch(createShoppingList(name));
  }

  addProductToList(listId: string, product: Product) {
    this.store.dispatch(addProductToShoppingList(listId, product));
  }

  removeProductFromList(listId: string, product: Product) {
    this.store.dispatch(removeProductFromShoppingList(listId, product));
  }

  changeName(listId: string, listName: string) {
    this.store.dispatch(changeListName(listId, listName));
  }
}
