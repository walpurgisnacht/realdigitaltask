import { ProductsState } from './../../store/reducers/products.reducer';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { Product } from '../../models/Product.interface';
import { loadProducts } from '../../store/actions/products.actions';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  products$: Observable<Product[]>;

  constructor(private store: Store<{ products: ProductsState }>) {
    this.products$ = this.store.select(state => state.products.products);
  }

  load(searchTerm: string, sort: string, maxResults: number) {
    this.store.dispatch(loadProducts(searchTerm, sort, maxResults));
  }
}
