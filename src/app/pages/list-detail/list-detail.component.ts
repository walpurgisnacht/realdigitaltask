import { ShoppingList } from './../../models/ShoppingList.interface';
import { ShoppingListsService } from './../../services/shopping-lists/shopping-lists.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { switchMap, take } from 'rxjs/operators';
import { stringify } from '@angular/core/src/util';

@Component({
  selector: 'app-list-detail',
  templateUrl: './list-detail.component.html',
  styleUrls: ['./list-detail.component.scss']
})
export class ListDetailComponent implements OnInit {

  list: ShoppingList;
  showEditName = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private shoppingListsService: ShoppingListsService
  ) { }

  ngOnInit() {
    this.route.paramMap.pipe().subscribe((params: ParamMap) => {
      this.shoppingListsService.shoppingLists$.pipe(take(1)).subscribe(lists => {
        this.list = lists.find(l => l.id === params.get('id'));
        if (!this.list) {
          this.router.navigate(['/not-found']);
        }
      });
    });
  }

  updateList() {
    this.shoppingListsService.shoppingLists$.pipe(take(1)).subscribe(lists => {
      this.list = lists.find(l => l.id === this.list.id);
    });
  }

  changeName() {
    if (this.list.name) {
      this.shoppingListsService.changeName(this.list.id, this.list.name);
      this.showEditName = false;
      this.updateList();
    }
  }

}
