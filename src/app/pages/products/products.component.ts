import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/products/products.service';
import { Product } from '../../models/Product.interface';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  constructor(private productsService: ProductsService) { }

  products$: Observable<Product[]>;
  maxResults = 10;
  searchTerm = '';
  sortBy = 'relevance';

  ngOnInit() {
    this.products$ = this.productsService.products$;
    this.searchProducts();
  }

  searchProducts() {
    this.productsService.load(this.searchTerm, this.sortBy, this.maxResults);
  }

}
